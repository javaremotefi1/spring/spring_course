package com.sda.spring_course.service;

import com.sda.spring_course.dto.TaskDto;
import com.sda.spring_course.exceptions.NotFound.TaskNotFoundException;
import com.sda.spring_course.model.Enums.TaskPriority;
import com.sda.spring_course.model.Enums.TaskStatus;
import com.sda.spring_course.model.Task;
import com.sda.spring_course.repository.TaskRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Service
@AllArgsConstructor
public class TaskService {

    private final TaskRepository taskRepository;

    public TaskDto saveTask(TaskDto newTaskDto) {

        Task newTask = toTaskModel(newTaskDto);

        return toTaskDto(taskRepository.save(newTask));
    }

    private TaskDto toTaskDto(Task task) {
        TaskDto taskDto = new TaskDto();
        taskDto.setTitle(task.getTitle());
        taskDto.setDescription(task.getDescription());
        taskDto.setStatus(task.getStatus());
        taskDto.setPriority(task.getPriority());
        taskDto.setBoardId(task.getBoardId());
        taskDto.setAssigneeId(task.getAssigneeId());
        taskDto.setReporterId(task.getReporterId());
        taskDto.setCreationDate(task.getCreationDate());
        taskDto.setEndDate(task.getEndDate());
        taskDto.setId(task.getId());

        System.out.println("\nTaskDto created: " );
        return taskDto;
    }

    private Task toTaskModel(TaskDto newTaskDto) {
        Task task = new Task();
        task.setTitle(newTaskDto.getTitle());
        task.setTitle(newTaskDto.getTitle());
        task.setDescription(newTaskDto.getDescription());
        task.setStatus(newTaskDto.getStatus());
        task.setPriority(newTaskDto.getPriority());
        task.setBoardId(newTaskDto.getBoardId());
        task.setAssigneeId(newTaskDto.getAssigneeId());
        task.setReporterId(newTaskDto.getReporterId());
        task.setCreationDate(LocalDate.now());
        task.setEndDate(newTaskDto.getEndDate());

        System.out.println("\nTask created: " );
        return task;
    }

    public Task editTask(String title, String description, TaskStatus status, TaskPriority priority) {
        Task taskToEdit = new Task();
        taskToEdit.setTitle(title);
        taskToEdit.setDescription(description);
        taskToEdit.setStatus(status);
        taskToEdit.setPriority(priority);

        return taskToEdit;
    }

    public TaskDto updateTaskData(Long taskId, TaskDto newTaskData) {
        Task taskToUpdate = taskRepository.findById(taskId).orElse(null);

        if(taskToUpdate != null) {
            if (taskToUpdate.getStatus() == TaskStatus.TODO) {

                taskToUpdate.setStatus(newTaskData.getStatus());
                taskToUpdate.setPriority(newTaskData.getPriority());
                taskToUpdate.setTitle(newTaskData.getTitle());
                taskToUpdate.setDescription(newTaskData.getDescription());
                taskToUpdate.setBoardId(newTaskData.getBoardId());
                taskToUpdate.setAssigneeId(newTaskData.getAssigneeId());
                taskToUpdate.setReporterId(newTaskData.getReporterId());

                System.out.println("\nTaskId " + taskId + " updated: " + taskToUpdate);
                return toTaskDto(taskRepository.save(taskToUpdate));
            }
        }
        return null;
    }

    public TaskDto updateTaskStatus(Long taskId, TaskStatus status) throws TaskNotFoundException {
        Task taskToUpdate = taskRepository.findById(taskId).orElse(null);
        if (taskToUpdate == null)
            throw new TaskNotFoundException(taskId);

        taskToUpdate.setStatus(status);
        return toTaskDto(taskRepository.save(taskToUpdate));
    }

    public TaskDto updateTaskPriority(Long taskId, TaskPriority priority) throws TaskNotFoundException {
        Task taskToUpdate = taskRepository.findById(taskId).orElse(null);
        if (taskToUpdate == null)
            throw new TaskNotFoundException(taskId);

        taskToUpdate.setPriority(priority);
        return toTaskDto(taskRepository.save(taskToUpdate));
    }

    public List<TaskDto> findTasksByBoardId(Long boardId) {
        List<Task> tasks = taskRepository.findAllByBoardId(boardId);
        List<TaskDto> taskDtoList = new ArrayList<>();
        for(Task task : tasks){
            taskDtoList.add(toTaskDto(task));
        }
        return taskDtoList;
    }

    public List<TaskDto> getAllTasks() {
        List<Task> tasks = taskRepository.findAll();
        List<TaskDto> taskDtos = new ArrayList<>();
        for(Task task : tasks){
            taskDtos.add(toTaskDto(task));
        }
        return taskDtos;
    }

    public List<TaskDto> getAllDoneTasks() {
        List<Task> tasks = taskRepository.findAllByStatus(TaskStatus.DONE);
        List<TaskDto> taskDtos = new ArrayList<>();

        for(Task task : tasks) {
            taskDtos.add(toTaskDto(task));
        }
        return taskDtos;
    }

    public TaskDto getTaskById(Long id) {
        Task task = taskRepository.findById(id).orElse(null);
        return task != null ? toTaskDto(task) : null;
    }

    public List<TaskDto> getTasksByStatus(TaskStatus status) {
        List<Task> tasks = taskRepository.findAllByStatus(status);
        List<TaskDto> taskDtos = new ArrayList<>();

        for(Task task : tasks){
            taskDtos.add(toTaskDto(task));
        }
        return taskDtos;
    }

    public List<TaskDto> getTasksByPriority(TaskPriority priority) {
        List<Task> tasks = taskRepository.findAllByPriority(priority);
        List<TaskDto> taskDtos = new ArrayList<>();

        for(Task task : tasks) {
            taskDtos.add(toTaskDto(task));
        }
        return taskDtos;
    }

    public List<TaskDto> getTasksByCreationDate(LocalDate creationDate) {
        List<Task> tasks = taskRepository.findAllByCreationDate(creationDate);
        List<TaskDto> taskDtos = new ArrayList<>();

        for(Task task : tasks) {
            taskDtos.add(toTaskDto(task));
        }
        return taskDtos;
    }

    public void markTaskAsDeleted(Long id) {
        Task task = taskRepository.findById(id).orElse(null);

        if(task != null) {
            task.setStatus(TaskStatus.DELETED);
            taskRepository.save(task);
        }
    }

    public List<TaskDto> findAssigneeIdAndStatus(Long userId, TaskStatus status) {
        List<Task> tasks = taskRepository.findAllByAssigneeIdAndStatus(userId, status);
        List<TaskDto> taskDtoList = new ArrayList<>();
        for(Task task : tasks) {
            taskDtoList.add(toTaskDto(task));
        }
        return taskDtoList;
    }

    public List<TaskDto> findTasksByTitle(String title) {
        List<Task> tasks = taskRepository.findAllByTitleLike("%" + title + "%");
        List<TaskDto> taskDtoList = new ArrayList<>();
        for(Task task: tasks) {
            taskDtoList.add(toTaskDto(task));
        }
        return taskDtoList;
    }
}
