package com.sda.spring_course.service;

import com.sda.spring_course.dto.responses.BoardDetailResponse;
import com.sda.spring_course.dto.BoardDto;
import com.sda.spring_course.dto.TaskDto;
import com.sda.spring_course.exceptions.BoardInCompleteTasksException;
import com.sda.spring_course.exceptions.NotFound.BoardNotFoundException;
import com.sda.spring_course.model.Board;
import com.sda.spring_course.model.Enums.BoardStatus;
import com.sda.spring_course.model.Enums.TaskStatus;
import com.sda.spring_course.repository.BoardRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Service
@AllArgsConstructor
public class BoardService {

    private final BoardRepository boardRepository;
    private final TaskService taskService;

    public BoardDto saveBoard(BoardDto newBoardDto) {

        Board newBoard = toBoardModel(newBoardDto);

        return toBoardDto(boardRepository.save(newBoard));
    }

    private BoardDto toBoardDto(Board board){
        BoardDto boardDto = new BoardDto();
        boardDto.setName(board.getName());
        boardDto.setDescription(board.getDescription());
        boardDto.setStartDate(board.getStartDate());
        boardDto.setEndDate(board.getEndDate());
        boardDto.setBoardStatus(board.getBoardStatus());
        boardDto.setId(board.getId());

        return boardDto;
    }

    private Board toBoardModel(BoardDto newBoardDto) {
        Board board = new Board();
        board.setId(newBoardDto.getId());
        board.setName(newBoardDto.getName());
        board.setDescription(newBoardDto.getDescription());
        board.setStartDate(newBoardDto.getStartDate());
        board.setEndDate(newBoardDto.getEndDate());
        board.setBoardStatus(newBoardDto.getBoardStatus());

        return board;
    }

    public Board closeBoard(Long boardId) throws BoardNotFoundException, BoardInCompleteTasksException {
        Board boardToClose = boardRepository.findById(boardId).orElse(null);
        if(boardToClose == null)
            throw new BoardNotFoundException(boardId);

        boolean boardIsCompleted = true;
        for(TaskDto task : taskService.findTasksByBoardId(boardId)) {
            if(task.getStatus() != TaskStatus.DONE) {
                boardIsCompleted = false;
                break;
            }
        }
        if(!boardIsCompleted) {
            throw new BoardInCompleteTasksException(boardId);
        }
        boardToClose.setBoardStatus(BoardStatus.CLOSED);
        return boardRepository.save(boardToClose);
    }

    public List<BoardDto> getAllBoards() {
        List<Board> boards = boardRepository.findAll();
        List<BoardDto> boardDtos = new ArrayList<>();
        for(Board board : boards){
            boardDtos.add(toBoardDto(board));
        }
        return boardDtos;
    }

    public List<Board> getBoardsByStatus(BoardStatus status) {
        return boardRepository.findAllByBoardStatus(status);
    }

    public BoardDto getBoardById(Long id) {
        Board board = boardRepository.findById(id).orElse(null);
        return board != null ? toBoardDto(board) : null; //What is this??
    }

    public Board createBoard(String name, String description, LocalDate startDate, LocalDate endDate) {
        Board boardToCreate = new Board();
        boardToCreate.setName(name);
        boardToCreate.setDescription(description);
        boardToCreate.setStartDate(startDate);
        boardToCreate.setEndDate(endDate);
        boardToCreate.setBoardStatus(BoardStatus.ACTIVE);
        System.out.println("\nBoard created: " );

        return boardToCreate;
    }

    public Board editBoard(String name, String description, LocalDate startDate, LocalDate endDate) {
        Board boardToEdit = new Board();
        boardToEdit.setName(name);
        boardToEdit.setDescription(description);
        boardToEdit.setStartDate(startDate);
        boardToEdit.setEndDate(endDate);

        return boardToEdit;
    }

    public Board updateBoardData(Long boardId, Board newBoardData) throws BoardNotFoundException {
        Board boardToUpdate = boardRepository.findById(boardId).orElse(null);
        if(boardToUpdate == null)
            throw new BoardNotFoundException(boardId);

        boardToUpdate.setName(newBoardData.getName());
        boardToUpdate.setDescription(newBoardData.getDescription());
        boardToUpdate.setBoardStatus(newBoardData.getBoardStatus());
        boardToUpdate.setStartDate(newBoardData.getStartDate());
        boardToUpdate.setEndDate(newBoardData.getEndDate());

        System.out.println("\nBoardId " + boardId + " updated: " + boardToUpdate);
        return null;
    }

    public void markBoardAsDeleted(Long id) throws BoardNotFoundException, BoardInCompleteTasksException {
        Board board = boardRepository.findById(id).orElse(null);

        if (board != null) {
                boolean boardIsCompleted = true;

                for(TaskDto task : taskService.findTasksByBoardId(id)) {
                    if(task.getStatus() != TaskStatus.DONE) {
                        boardIsCompleted = false;
                        break;
                    }
                }

                if(!boardIsCompleted) {
                    throw new BoardInCompleteTasksException(id);
                }

                board.setBoardStatus(BoardStatus.DELETED);
                boardRepository.save(board);

        } else
            throw new BoardNotFoundException(id);
    }

    public BoardDto updateBoardStatus(Long id, Board board) {
        Board boardToUpdate = boardRepository.findById(id).orElse(null);

        if(boardToUpdate != null) {
            boardToUpdate.setBoardStatus(board.getBoardStatus());
            return toBoardDto(boardRepository.save(boardToUpdate));
        }
        return null;
    }

    public BoardDetailResponse getBoardDetail(Long boardId) {
        Board board = boardRepository.findById(boardId).orElse(null);

        if(board != null){
            List<TaskDto> tasks = taskService.findTasksByBoardId(boardId);
            return toBoardDetailResponse(board, tasks);
        }
        return null;
    }

    private BoardDetailResponse toBoardDetailResponse(Board board, List<TaskDto> tasks) {
        BoardDetailResponse boardDetailResponse = new BoardDetailResponse();
        boardDetailResponse.setId(board.getId());
        boardDetailResponse.setName(board.getName());
        boardDetailResponse.setDescription(board.getDescription());
        boardDetailResponse.setStartDate(board.getStartDate());
        boardDetailResponse.setEndDate(board.getEndDate());
        boardDetailResponse.setBoardStatus(board.getBoardStatus());
        boardDetailResponse.setTaskDtoList(tasks);

        return boardDetailResponse;
    }

    public List<BoardDto> findBoardsByNameOrDescription(String text) {
        List<Board> boards = boardRepository.findAllByNameLikeOrDescriptionLike("%" + text + "%");
        List<BoardDto> boardDtoList = new ArrayList<>();
        for(Board board : boards){
            boardDtoList.add(toBoardDto(board));
        }
        return boardDtoList;
    }
}
