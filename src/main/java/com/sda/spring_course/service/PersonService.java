package com.sda.spring_course.service;

import com.sda.spring_course.dto.PersonDto;
import com.sda.spring_course.exceptions.NotFound.PersonNotFoundException;
import com.sda.spring_course.model.Person;
import com.sda.spring_course.repository.PersonRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@AllArgsConstructor
public class PersonService {

    private final PersonRepository personRepository;

    public PersonDto savePerson(PersonDto newPersonDto) {
        Person newPerson = personDtoToPerson(newPersonDto);
        return toPersonDto(personRepository.save(newPerson));
    }

    public List<PersonDto> getAllPersons() {
        List<Person> personList = personRepository.findAll();
        List<PersonDto> personDtos = new ArrayList<>();

        for(Person person : personList) {
            personDtos.add(toPersonDto(person));
        }
        return personDtos;
    }

    public List<PersonDto> getPersonBySurname(String surname) {
        List<Person> personList = personRepository.findAllBySurname(surname);
        List<PersonDto> personDtos = new ArrayList<>();

        for(Person person : personList) {
            personDtos.add(toPersonDto(person));
        }
        return personDtos;
    }

    public PersonDto getPersonById(Long id) {
        Person person = personRepository.findById(id).orElse(null);
        return person != null ? toPersonDto(person) : null;
    }

    public Person personDtoToPerson(PersonDto personDto){
        Person person = new Person();
        person.setId(personDto.getId());
        person.setName(personDto.getName());
        person.setSurname(personDto.getSurname());
        person.setUserId(personDto.getUserId());
        System.out.println("\nPerson created: " + person);

        return person;
    }

    public PersonDto toPersonDto(Person person) {
        PersonDto personDto = new PersonDto();
        personDto.setId(person.getId());
        personDto.setName(person.getName());
        personDto.setSurname(person.getSurname());
        personDto.setUserId(person.getUserId());
        System.out.println("\nPersonDto created: " + personDto);

        return personDto;
    }

    public PersonDto updatePersonData(Long personId, PersonDto newPersonData) throws PersonNotFoundException {
        Person personToUpdate = personRepository.findById(personId).orElseThrow(() -> new PersonNotFoundException(personId));

        if(personToUpdate != null) {
            personToUpdate.setName(newPersonData.getName());
            personToUpdate.setSurname(newPersonData.getSurname());
            personToUpdate.setUserId(newPersonData.getUserId());

            System.out.println("\nPersonId " + personId + " updated: " + personToUpdate);
            return toPersonDto(personRepository.save(personToUpdate));
        }
        return null;
    }

}
