package com.sda.spring_course.service;

import com.sda.spring_course.dto.TaskDto;
import com.sda.spring_course.dto.UserDto;
import com.sda.spring_course.dto.responses.UserTaskResponse;
import com.sda.spring_course.exceptions.NotFound.UserNotFoundException;
import com.sda.spring_course.exceptions.UserYoungerThan18Exception;
import com.sda.spring_course.exceptions.UsernameOrEmailAlreadyExistsException;
import com.sda.spring_course.model.Enums.TaskStatus;
import com.sda.spring_course.model.User;
import com.sda.spring_course.model.Enums.UserStatus;
import com.sda.spring_course.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

//Check if user is above 18 then create user record otherwise throw an exception

@Service
@AllArgsConstructor
public class UserService {

    //Injecting dependency by field mainly used to test classes
    //@Autowired
    //private UserRepository userRepository;

    //Injecting dependency by constructor
    private final UserRepository userRepository;
    private final TaskService taskService;

    private void validateUserName(UserDto userDto) throws UsernameOrEmailAlreadyExistsException {
        User existingUserByUsername = userRepository.findByUsernameAndUserStatus(userDto.getUsername(), userDto.getUserStatus());
        User existingUserByEmail = userRepository.findByEmailAndUserStatus(userDto.getEmail(), userDto.getUserStatus());

        if (existingUserByUsername != null || existingUserByEmail != null) {
            throw new UsernameOrEmailAlreadyExistsException(userDto.getUsername() + " - " + userDto.getEmail());
        }
    }

    private boolean isAbove18(Integer age) {
        return age > 18;
    }

    public void validateUserAge(UserDto userDto) throws UserYoungerThan18Exception {
        if (!isAbove18(userDto.getAge())) {
            throw new UserYoungerThan18Exception(String.valueOf(userDto.getAge()));
        }
    }

    public UserDto saveUser(UserDto newUserDto) throws UsernameOrEmailAlreadyExistsException, UserYoungerThan18Exception {
        validateUserName(newUserDto);
        validateUserAge(newUserDto);

        User newUser = userDtoToUser(newUserDto);

        return toUserDto(userRepository.save(newUser));
    }

    private User userDtoToUser(UserDto userDto) {
        User user = new User();
        user.setAge(userDto.getAge());
        user.setEmail(userDto.getEmail());
        user.setUsername(userDto.getUsername());
        user.setPassword(userDto.getPassword());
        user.setPersonId(userDto.getPersonId());
        user.setStartDate(userDto.getStartDate());
        user.setEndDate(userDto.getEndDate());
        user.setUserStatus(userDto.getUserStatus());

        System.out.println("\nUser created: " + user);
        return user;
    }
    private UserDto toUserDto(User user) {
        UserDto userDto = new UserDto();
        userDto.setAge(user.getAge());
        userDto.setEmail(user.getEmail());
        userDto.setUsername(user.getUsername());
        userDto.setPassword(user.getPassword());
        userDto.setPersonId(user.getPersonId());
        userDto.setStartDate(user.getStartDate());
        userDto.setEndDate(user.getEndDate());
        userDto.setUserStatus(user.getUserStatus());

        System.out.println("\nUserDto created: " + userDto);
        return userDto;
    }

    public UserDto updateUserData(Long userId, UserDto newUserData) throws UserNotFoundException,UserYoungerThan18Exception {
        User userToUpdate = userRepository.findById(userId).orElseThrow(() -> new UserNotFoundException(userId));

        validateUserAge(newUserData);

        if(userToUpdate != null) {
            userToUpdate.setUsername(newUserData.getUsername());
            userToUpdate.setPassword(newUserData.getPassword());
            userToUpdate.setAge(newUserData.getAge());
            userToUpdate.setEmail(newUserData.getEmail());
            userToUpdate.setUserStatus(newUserData.getUserStatus());
            userToUpdate.setPersonId(newUserData.getPersonId());

            System.out.println("\nUserId " + userId + " updated: " + userToUpdate);
            return toUserDto(userRepository.save(userToUpdate));

        }
        return null;
    }

    public UserDto updateUserStatus(Long userId, UserStatus userStatus) throws UserNotFoundException {
        User userToUpdate = userRepository.findById(userId).orElse(null);
        if (userToUpdate == null)
            throw new UserNotFoundException(userId);

        userToUpdate.setUserStatus(userStatus);
        return toUserDto(userRepository.save(userToUpdate));
    }

    public List<UserDto> getAllUsers() {
        List<User> users = userRepository.findAll();
        List<UserDto> userDtos = new ArrayList<>();

        for(User user : users) {
            userDtos.add(toUserDto(user));
        }
        return userDtos;
    }

    public List<UserDto> getUsersByUserStatus(UserStatus userStatus) {
        List<User> users = userRepository.findAllByUserStatus(userStatus);
        List<UserDto> userDtos = new ArrayList<>();

        for(User user : users) {
            userDtos.add(toUserDto(user));
        }
        return userDtos;
    }

    public void markUserAsDeleted(Long id) {
        User user = userRepository.findById(id).orElse(null);

        if(user != null) {
            user.setUserStatus(UserStatus.DELETED);
            userRepository.save(user);
        }
        //userRepository.delete(user);
    }

    public UserTaskResponse getUserTasksFilterByStatus(Long userId, TaskStatus status) {
        User user = userRepository.findById(userId).orElse(null);

        if(user != null) {
            List<TaskDto> taskDtoList = taskService.findAssigneeIdAndStatus(userId, status);
            UserTaskResponse userTaskResponse = new UserTaskResponse();
            userTaskResponse.setUsername(user.getUsername());
            userTaskResponse.setEmail(user.getEmail());
            userTaskResponse.setTasks(taskDtoList);
            return userTaskResponse;
        }
        return null;
    }

    public UserDto getUserByUsername(String username) throws UserNotFoundException {
        Optional<User> userOptional = userRepository.findUserByUsername(username);

        if (userOptional.isPresent()) {
            return toUserDto(userOptional.get());
        } else {
            throw new UserNotFoundException(userOptional.get().getId());
        }
    }

    public UserDto getUserById(Long id) {
        User user = userRepository.findById(id).orElse(null);
        return user != null ? toUserDto(user) : null;
    }
}
