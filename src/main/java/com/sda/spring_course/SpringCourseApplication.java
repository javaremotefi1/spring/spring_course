package com.sda.spring_course;

import com.sda.spring_course.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringCourseApplication implements CommandLineRunner {

	@Autowired
	private PersonService personService;
	@Autowired
	private BoardService boardService;
	@Autowired
	private TaskService taskService;
	@Autowired
	private UserService userService;

	@Value("${spring.application.name}")
	private String message;

	@Value("${welcome.message}")
	private String welcomeMessage;

	public static void main(String[] args) {

		SpringApplication.run(SpringCourseApplication.class, args);
	}

    @Override
	public void run(String... args) throws Exception {
		System.out.println("This is a message from Run method using command line runner");
		System.out.println(message);
		System.out.println(welcomeMessage);

		/*//creating boards
		Board board1 = boardService.createBoard("Board", "First board", LocalDate.of(2024,6,16), LocalDate.of(2024,8,19));
		Board board2 = boardService.createBoard("BoardVer2", "Second board", LocalDate.of(2024,7,3), LocalDate.of(2024,8,30));

		//creating person1
		Person person1 = personService.createPerson("Alex", "Biden", 1L);

		Person newPerson1 = personService.editPerson("Joe", "Biden", 1L);

		//Updating person data
		try {
			personService.updatePersonData(person1.getId(), newPerson1);
		} catch (PersonNotFoundException e) {
			System.out.println(e.getMessage());
        }


        //creating person2
		Person person2 = personService.createPerson("Peter", "Trump", 2L);

		//creating users
		UserDto user1 = userService.createUser("alex12", "password", 19, "sda@sda.ee", 1L, LocalDate.of(2024,6,3), LocalDate.of(2024,7,3));
		UserDto newUser1 = userService.editUser("alex123", "password", 20, "sda@sda.ee", 1L, LocalDate.of(2024,7,5));

		//Updating user data
        try {
            userService.updateUserData(user1.getId(), newUser1);
        } catch (UserNotFoundException | UserYoungerThan18Exception e) {
			System.out.println(e.getMessage());
        }

        //Creating user2
        UserDto user2 = userService.createUser( "PeterG", "password", 55, "sda@sda.com", 2L, LocalDate.of(2024,5,1), LocalDate.of(2024,7,3));

		//Creating task1
		Task task1 = taskService.createTask("Spring",
						"Finish course",
						TaskStatus.TODO, TaskPriority.URGENT);

		task1.setBoardId(1L);
		task1.setReporterId(1L);
		task1.addAssigneeId(1L);
		task1.addAssigneeId(2L);

		taskService.saveTask(task1);

		//Creating task2
		Task task2 = taskService.createTask("Wash clothes",
				"Tidy whities first, then color clothes",
							TaskStatus.TODO, TaskPriority.NORMAL);

		task2.setBoardId(2L);
		task2.setReporterId(1L);
		task2.addAssigneeId(1L);
		task2.addAssigneeId(2L);

		taskService.saveTask(task2);

		//Updating task taskStatus
        try {
            taskService.updateTaskStatus(task1.getId(), TaskStatus.DONE);
        } catch (TaskNotFoundException e) {
			System.out.println(e.getMessage());;
        }

		//Creating task3
		Task task3 = taskService.editTask(null,
				"Tidy whities first, then color clothes, but they are done",
				TaskStatus.DONE, TaskPriority.MODERATE);

		task3.setBoardId(1L);
		task3.setReporterId(1L);
		task3.setBoardId(2L);
		task3.addAssigneeId(1L);
		task3.addAssigneeId(2L);

		//Updating task1 data
        try {
            taskService.updateTaskData(task2.getId(), task3);
        } catch (TaskNotFoundException e) {
			System.out.println(e.getMessage());
        }

		//Closing board
        try {
            Board boardToClose = boardService.closeBoard(1L);
			System.out.println(boardToClose);
        } catch (BoardNotFoundException | BoardInCompleteTasksException e) {
			System.out.println(e.getMessage());
        }*/

        //Person selectedPerson = personRepository.getOne(1L);
		//System.out.println(selectedPerson);
		System.out.println("\n############################" +
				"			\n#Application run successful#" +
				"			\n############################");
	}
}
