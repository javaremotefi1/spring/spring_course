package com.sda.spring_course.dto;

import com.sda.spring_course.model.Enums.BoardStatus;
import jakarta.persistence.*;
import lombok.Data;

import java.time.LocalDate;

@Data
public class BoardDto {

    private Long id;

    private String name;

    private String description;

    private LocalDate startDate;

    private LocalDate endDate;

    @Enumerated(EnumType.STRING)
    private BoardStatus boardStatus;
}
