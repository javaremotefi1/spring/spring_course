package com.sda.spring_course.dto.responses;

import com.sda.spring_course.dto.TaskDto;
import com.sda.spring_course.model.Enums.BoardStatus;
import lombok.Data;

import java.time.LocalDate;
import java.util.List;

@Data
public class BoardDetailResponse {
    private Long id;

    private String name;

    private String description;

    private LocalDate startDate;

    private LocalDate endDate;

    private BoardStatus boardStatus;

    private List<TaskDto> taskDtoList;
}
