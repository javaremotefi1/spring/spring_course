package com.sda.spring_course.dto.responses;

import com.sda.spring_course.dto.TaskDto;
import lombok.Data;

import java.util.List;

@Data
public class UserTaskResponse {
    private String username;
    private String email;
    private List<TaskDto> tasks;
}
