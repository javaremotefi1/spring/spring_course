package com.sda.spring_course.dto;

import lombok.Data;

@Data
public class PersonDto {

    private Long id;

    private String name;

    private String surname;

    private Long userId;
}
