package com.sda.spring_course.dto;

import com.sda.spring_course.model.Enums.UserStatus;
import jakarta.persistence.*;
import lombok.Data;

import java.time.LocalDate;

@Data
public class UserDto {


    private Long id;

    private String username;

    private String password;

    private Integer age;

    private String email;

    private Long personId;

    private LocalDate startDate;

    private LocalDate endDate;

    @Enumerated(EnumType.STRING)
    private UserStatus userStatus;
}
