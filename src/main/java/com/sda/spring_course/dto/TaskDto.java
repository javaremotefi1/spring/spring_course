package com.sda.spring_course.dto;

import com.sda.spring_course.model.Enums.TaskPriority;
import com.sda.spring_course.model.Enums.TaskStatus;
import jakarta.persistence.*;
import lombok.Data;

import java.time.LocalDate;

@Data
public class TaskDto {

    private Long id;

    private String title;

    private String description;

    @Enumerated(EnumType.STRING)
    private TaskStatus status;

    @Enumerated(EnumType.STRING)
    private TaskPriority priority;

    private Long reporterId;

    private Long assigneeId;

    private Long boardId;

    private LocalDate creationDate;

    private LocalDate endDate;
}
