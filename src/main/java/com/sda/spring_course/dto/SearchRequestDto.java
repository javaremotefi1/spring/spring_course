package com.sda.spring_course.dto;

import lombok.Data;

@Data
public class SearchRequestDto {
    String taskTitle;
    String text;
}
