package com.sda.spring_course.controller;

import com.sda.spring_course.dto.responses.BoardDetailResponse;
import com.sda.spring_course.dto.BoardDto;
import com.sda.spring_course.exceptions.BoardInCompleteTasksException;
import com.sda.spring_course.exceptions.NotFound.BoardNotFoundException;
import com.sda.spring_course.model.Board;
import com.sda.spring_course.model.Enums.BoardStatus;
import com.sda.spring_course.service.BoardService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;


import java.util.List;

@RestController
@AllArgsConstructor
public class BoardController {

    private final BoardService boardService;

    //Let's implement an api that will return all boards
    @GetMapping(path = "/boards")
    public List<BoardDto> getAllBoards() {
        return boardService.getAllBoards();
    }

    @GetMapping(path = "/boards/boards-by-status")
    public List<Board> getBoardsByStatus(@RequestParam String status) {
        return boardService.getBoardsByStatus(BoardStatus.valueOf(status));
    }

    @GetMapping(path = "/boards/{id}")
    public BoardDto getBoardById(@PathVariable("id") Long id) {
        return boardService.getBoardById(id);
    }

    @DeleteMapping(path = "/boards/{id}")
    public void deleteBoard(@PathVariable("id") Long id) throws BoardNotFoundException, BoardInCompleteTasksException {
        boardService.markBoardAsDeleted(id);
    }

    @PutMapping(path = "/boards/boards-by-status/{id}")
    public BoardDto updateBoardStatus(@PathVariable("id") Long id, @RequestBody Board board) {
        return boardService.updateBoardStatus(id, board);
    }

    @PostMapping(path = "/boards")
    public BoardDto createBoard(@RequestBody BoardDto boardDto) {
        return boardService.saveBoard(boardDto);
    }

    @GetMapping(path = "/boards/{id}/tasks")
    public BoardDetailResponse getBoardDetail(@PathVariable("id") Long boardId){
        return boardService.getBoardDetail(boardId);
    }
}
