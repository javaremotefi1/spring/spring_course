package com.sda.spring_course.controller;

import com.sda.spring_course.dto.PersonDto;
import com.sda.spring_course.exceptions.NotFound.PersonNotFoundException;
import com.sda.spring_course.service.PersonService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@AllArgsConstructor
public class PersonController {

    private final PersonService personService;

    @GetMapping(path = "/persons")
    public List<PersonDto> getAllPersons() {
        return personService.getAllPersons();
    }

    @GetMapping(path = "/persons/{surname}")
    public List<PersonDto> findAllBySurname(@PathVariable("surname") String surname){
        return personService.getPersonBySurname(surname);
    }

    @GetMapping(path = "/person/{id}")
    public PersonDto findPersonById(@PathVariable("id") Long id){
        return personService.getPersonById(id);
    }

    @PostMapping(path = "/persons")
    public PersonDto createPerson(@RequestBody PersonDto personDto) {
        return personService.savePerson(personDto);
    }

    @PutMapping(path = "/persons/{id}")
    public PersonDto updatePerson(@PathVariable("id") Long id, @RequestBody PersonDto personDto) throws PersonNotFoundException {
        return personService.updatePersonData(id, personDto);
    }
}
