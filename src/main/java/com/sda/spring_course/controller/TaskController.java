package com.sda.spring_course.controller;

import com.sda.spring_course.dto.TaskDto;
import com.sda.spring_course.model.Enums.TaskPriority;
import com.sda.spring_course.model.Enums.TaskStatus;
import com.sda.spring_course.service.TaskService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;

@RestController
@AllArgsConstructor
public class TaskController {

    private final TaskService taskService;

    @GetMapping(path = "/tasks")
    public List<TaskDto> getAllTasks() {
        return taskService.getAllTasks();
    }

    @GetMapping(path = "/tasks/done")
    public List<TaskDto> getAllDoneTasks() {
        return taskService.getAllDoneTasks();
    }

    @GetMapping(path = "/tasks/{id})")
    public TaskDto findTaskById(@PathVariable("id") Long id){
        return taskService.getTaskById(id);
    }

    @GetMapping(path = "/tasks/tasks-by-status")
    public List<TaskDto> getTasksByStatus(@RequestParam String status) {
        return taskService.getTasksByStatus(TaskStatus.valueOf(status));
    }

    @GetMapping(path = "tasks/tasks-by-priority")
    public List<TaskDto> getTasksByPriority(@RequestParam String priority) {
        return taskService.getTasksByPriority(TaskPriority.valueOf(priority));
    }

    @GetMapping(path = "/tasks/tasks-by-creation-date")
    public List<TaskDto> getTasksByCreationDate(@RequestParam String creationDate){
        LocalDate parsedDate = LocalDate.parse(creationDate);
        return taskService.getTasksByCreationDate(parsedDate);
    }

    @PostMapping(path = "/tasks")
    public TaskDto createTask(@RequestBody TaskDto task){
        return taskService.saveTask(task);
    }

    @DeleteMapping(path = "/tasks/{id}")
    public void deleteTask(@PathVariable("id") Long id){
        taskService.markTaskAsDeleted(id);
    }

    @PutMapping(path = "/tasks/{id}")
    public TaskDto updateTask(@PathVariable("id") Long id, @RequestBody TaskDto taskDto) {
        return taskService.updateTaskData(id, taskDto);
    }
}
