package com.sda.spring_course.controller;

import com.sda.spring_course.dto.BoardDto;
import com.sda.spring_course.dto.SearchRequestDto;
import com.sda.spring_course.dto.TaskDto;
import com.sda.spring_course.dto.UserDto;
import com.sda.spring_course.exceptions.UserYoungerThan18Exception;
import com.sda.spring_course.exceptions.UsernameOrEmailAlreadyExistsException;
import com.sda.spring_course.service.BoardService;
import com.sda.spring_course.service.TaskService;
import com.sda.spring_course.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@AllArgsConstructor
@RequestMapping("/v1")
public class MainController {

    private final BoardService boardService;
    private final TaskService taskService;
    private final UserService userService;

    @GetMapping(path = "/")
    public String showMainPage(final ModelMap modelMap,
                               @PathVariable("message") String message,
                               @RequestParam("name") String name){
        modelMap.addAttribute("message", message);
        modelMap.addAttribute("name", name);
        return "index";
    }

    @GetMapping(path = "/api/boards")
    public String getAllBoards(final ModelMap modelMap) {
        modelMap.addAttribute("boards", boardService.getAllBoards());
        return "boards";
    }

    @GetMapping(path = "/api/board/{id}")
    public String getBoardById(@PathVariable("id") Long id, final ModelMap modelMap) {
        modelMap.addAttribute("boards", boardService.getBoardById(id));
        return "boards";
    }

    @GetMapping(path = "/api/tasks")
    public String getAllTasks(final ModelMap modelMap) {
        modelMap.addAttribute("tasks", taskService.getAllTasks());
        return "tasks";
    }

    @GetMapping(path = "/api/task/{id}")
    public String getTaskById(@PathVariable("id") Long id, final ModelMap modelMap) {
        modelMap.addAttribute("tasks", taskService.getTaskById(id));
        return "tasks";
    }

    @GetMapping(path = "/api/board/{id}/tasks")
    public String getTasksById(@PathVariable("id") Long boardId, final ModelMap modelMap) {
        modelMap.addAttribute("tasks", taskService.findTasksByBoardId(boardId));
        return "tasks";
    }

    @GetMapping(path = "/boards/create")
    public String showCreatedBoard(final ModelMap modelMap){
        BoardDto boardDto = new BoardDto();
        modelMap.addAttribute("create-board");
        return "redirect:/v1/api/boards";
    }

    @PostMapping(path = "/api/boards")
    public String createBoard(
            @ModelAttribute("create-board") BoardDto boardDto) {
        BoardDto boardDto1 = boardService.saveBoard(boardDto);
        return "redirect:/v1/api/boards";
    }

    @GetMapping(path = "/tasks/create")
    public String showCreatedTask(final ModelMap modelMap){
        TaskDto taskDto = new TaskDto();
        modelMap.addAttribute("create-task");
        return "redirect:/v1/api/tasks";
    }

    @PostMapping(path = "/api/tasks")
    public String createTask(
            @ModelAttribute("create-task") TaskDto taskDto) {
        TaskDto taskDto1 = taskService.saveTask(taskDto);
        return "redirect:/v1/api/tasks";
    }

    @GetMapping("/users/create")
    public String showCreateUserForm(final ModelMap modelMap) {
        UserDto userDto = new UserDto();
        modelMap.addAttribute("create-user", userDto);
        return "create-user";
    }

    @PostMapping(path = "/api/users")
    public String createUser(
            @ModelAttribute("create-user") UserDto userDto) {
        try{
            UserDto newUser = userService.saveUser(userDto);
        } catch (UsernameOrEmailAlreadyExistsException e) {
            throw new RuntimeException(e.getMessage());
        } catch (UserYoungerThan18Exception e) {
            throw new RuntimeException(e.getMessage());
        }
        return "redirect:/v1/api/users";
    }

    @GetMapping("/tasks/search")
    public String showTaskSearchForm(final ModelMap modelMap) {
        SearchRequestDto searchRequestDto = new SearchRequestDto();
        modelMap.addAttribute("searchRequestDto", searchRequestDto);
        return "search-form";
    }

    @PostMapping(path = "/api/tasks/search")
    public String createTaskSearchCriteria(
            @ModelAttribute("searchRequestDto") SearchRequestDto searchRequestDto,
            final ModelMap modelMap) {
        List <TaskDto> tasks = taskService.findTasksByTitle(searchRequestDto.getTaskTitle());
        modelMap.addAttribute("tasks", tasks);
        return "tasks";
    }

    @GetMapping("/boards/search")
    public String showBoardSearchForm(final ModelMap modelMap) {
        SearchRequestDto searchRequestDto = new SearchRequestDto();
        modelMap.addAttribute("searchRequestDto", searchRequestDto);
        return "boards-search-form";
    }

    @PostMapping(path = "/api/boards/search")
    public String createBoardSearchCriteria(
            @ModelAttribute("searchRequestDto") SearchRequestDto searchRequestDto,
            final ModelMap modelMap) {
        List <BoardDto> boards = boardService.findBoardsByNameOrDescription(searchRequestDto.getText());
        modelMap.addAttribute("boards", boards);
        return "boards";
    }
}
