package com.sda.spring_course.controller;

import com.sda.spring_course.dto.UserDto;
import com.sda.spring_course.dto.responses.UserTaskResponse;
import com.sda.spring_course.exceptions.NotFound.UserNotFoundException;
import com.sda.spring_course.exceptions.UserYoungerThan18Exception;
import com.sda.spring_course.exceptions.UsernameOrEmailAlreadyExistsException;
import com.sda.spring_course.model.Enums.TaskStatus;
import com.sda.spring_course.model.Enums.UserStatus;
import com.sda.spring_course.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@AllArgsConstructor
public class UserController {

    private final UserService userService;

    @GetMapping(path = "/users")
    public List<UserDto> getAllUsers(){
        return userService.getAllUsers();
    }

    @GetMapping(path = "/users/users-by-status")
    public List<UserDto> getUsersByUserStatus(@RequestParam String userStatus){
        return userService.getUsersByUserStatus(UserStatus.valueOf(userStatus));
    }

    @GetMapping(path = "users/{username}")
    public UserDto findUserByUsername(@PathVariable("username") String username) throws UserNotFoundException {
        return userService.getUserByUsername(username);
    }

    @GetMapping(path = "users/{id}")
    public UserDto findUserById(@PathVariable("id") Long id) {
        return userService.getUserById(id);
    }

    @PostMapping(path = "/users")
    public UserDto createUser(@RequestBody UserDto userDto) throws UsernameOrEmailAlreadyExistsException, UserYoungerThan18Exception {
        return userService.saveUser(userDto);
    }

    @DeleteMapping(path = "/users/{id}")
    public void deleteUser(@PathVariable("id") Long id) {
        userService.markUserAsDeleted(id);
    }

    @PutMapping(path = "/users/{id}")
    public UserDto updateUser(@PathVariable("id") Long id, @RequestBody UserDto userDto)
            throws UserNotFoundException, UserYoungerThan18Exception {
        return userService.updateUserData(id, userDto);
    }

    @GetMapping(path = "/users/{id}/tasks")
    public UserTaskResponse getUserTasks(
            @PathVariable("id") Long userId,
            @RequestParam TaskStatus status){
        return userService.getUserTasksFilterByStatus(userId, status);
    }
}
