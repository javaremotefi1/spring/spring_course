package com.sda.spring_course.model.Enums;

public enum TaskPriority {
    NORMAL,
    MODERATE,
    URGENT;
}
