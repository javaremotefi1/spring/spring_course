package com.sda.spring_course.model.Enums;

public enum UserStatus {
    ACTIVE,
    INACTIVE,
    DELETED
}
