package com.sda.spring_course.model.Enums;

public enum BoardStatus {
    ACTIVE,
    CLOSED,
    DELETED
}
