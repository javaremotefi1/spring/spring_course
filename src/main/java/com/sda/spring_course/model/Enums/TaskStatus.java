package com.sda.spring_course.model.Enums;

public enum TaskStatus {
    TODO,
    IN_PROGRESS,
    IN_REVIEW,
    DONE,
    DELETED;
}
