package com.sda.spring_course.model;

import com.sda.spring_course.model.Enums.TaskPriority;
import com.sda.spring_course.model.Enums.TaskStatus;
import jakarta.persistence.*;
import lombok.Data;

import java.time.LocalDate;

@Entity
@Data
public class Task {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String title;

    private String description;

    @Enumerated(EnumType.STRING)
    private TaskStatus status;//Needs Enum

    @Enumerated(EnumType.STRING)
    private TaskPriority priority;//Needs Enum

    private Long reporterId;

    private Long assigneeId;

    private Long boardId;

    private LocalDate creationDate;

    private LocalDate endDate;

    //This class belongs to what board?
    //Who is doing this task?
}
