package com.sda.spring_course.repository;

import com.sda.spring_course.model.User;
import com.sda.spring_course.model.Enums.UserStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    User findByUsernameAndUserStatus(String username, UserStatus status);

    User findByEmailAndUserStatus(String email, UserStatus status);

    List<User> findAllByUserStatus(UserStatus userStatus);

    Optional <User> findUserByUsername(String username);
}
