package com.sda.spring_course.repository;

import com.sda.spring_course.model.Board;
import com.sda.spring_course.model.Enums.BoardStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BoardRepository extends JpaRepository<Board, Long> {

    List<Board> findAllByBoardStatus(BoardStatus boardStatus);

    List<Board> findAllByNameLike(String name);

    List<Board> findAllByDescriptionLike(String description);

    @Query(value = "SELECT * FROM board ts WHERE ts.name LIKE %:text% OR ts.description LIKE %:text%", nativeQuery = true)
    List<Board> findAllByNameLikeOrDescriptionLike(String text);
}
