package com.sda.spring_course.repository;

import com.sda.spring_course.model.Person;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PersonRepository extends JpaRepository<Person, Long> {

    List<Person> findAllBySurname(String surname);

    //We would like to implement a task management like Trello
    //1. We would like to implement and design required models
        //1.1 We need to have user Model
        //1.2 We need to have Task Model
        //1.3 We need to have Board Model
    //2. Let implement repository interface for each Model

}
