package com.sda.spring_course.repository;

import com.sda.spring_course.model.Enums.TaskPriority;
import com.sda.spring_course.model.Enums.TaskStatus;
import com.sda.spring_course.model.Task;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface TaskRepository extends JpaRepository<Task, Long> {

    List<Task> findAllByBoardId(Long boardId);

    List<Task> findAllByStatus(TaskStatus status);

    List<Task> findAllByCreationDate(LocalDate creationDate);

    List<Task> findAllByAssigneeIdAndStatus(Long userId, TaskStatus status);

    List<Task> findAllByPriority(TaskPriority priority);

    List<Task> findAllByTitleLike(String title);

    @Query(value = "SELECT * FROM tasks ts WHERE ts.title LIKE %:title%", nativeQuery = true)
    List<Task> findAllByNamelikeSomething(@Param("title") String title);
}
