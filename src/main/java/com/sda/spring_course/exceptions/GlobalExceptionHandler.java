package com.sda.spring_course.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(UserYoungerThan18Exception.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public String handleUserYoungerThan18Exception(
            UserYoungerThan18Exception exception, Model model){
        model.addAttribute("errorMessage", exception.getMessage());
        return "user-age-exception";
    }

    @ExceptionHandler(UsernameOrEmailAlreadyExistsException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public String handleUsernameOrEmailAlreadyExistsException(
            UsernameOrEmailAlreadyExistsException exception, Model model){
        model.addAttribute("errorMessage", exception.getMessage());
        return "user-username-taken-exception";
    }

    @ExceptionHandler(RuntimeException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public String internalException(Model model) {
        model.addAttribute("errorMessage", "Internal error occurred, please contact" +
                "your admin");
        return "internal-server-error";
    }
}
