package com.sda.spring_course.exceptions;

public class UserYoungerThan18Exception extends Throwable {
    public UserYoungerThan18Exception(String message) {
        super("\nUser is younger than 18 years of age.");
    }
}
