package com.sda.spring_course.exceptions;

public class BoardInCompleteTasksException extends Throwable {
    public BoardInCompleteTasksException(Long boardId) {
        super("Board contains at least one incomplete task: " + boardId);
    }
}
