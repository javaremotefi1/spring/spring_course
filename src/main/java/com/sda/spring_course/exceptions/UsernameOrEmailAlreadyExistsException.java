package com.sda.spring_course.exceptions;

public class UsernameOrEmailAlreadyExistsException extends Throwable {
    public UsernameOrEmailAlreadyExistsException(String message) {
        super("\nUsername or email already exists.");
    }
}
