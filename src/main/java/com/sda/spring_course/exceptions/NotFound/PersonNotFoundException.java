package com.sda.spring_course.exceptions.NotFound;

public class PersonNotFoundException extends Throwable {
    public PersonNotFoundException(Long personId) {
            super("Person not found by Id: " + personId);
    }
}
