package com.sda.spring_course.exceptions.NotFound;

public class BoardNotFoundException extends Throwable {
    public BoardNotFoundException(Long boardId) {
        super("Board not found by Id: " + boardId);
    }
}
