package com.sda.spring_course.exceptions.NotFound;

public class UserNotFoundException extends Throwable {
    public UserNotFoundException(Long userId) {
        super("User not found by Id: " + userId);
    }
}
