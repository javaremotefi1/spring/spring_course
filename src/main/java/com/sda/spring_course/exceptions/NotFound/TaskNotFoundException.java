package com.sda.spring_course.exceptions.NotFound;

public class TaskNotFoundException extends Throwable {
    public TaskNotFoundException(Long taskId) {
        super("Task not found by Id: " + taskId);
    }
}
